require 'net/http'
require 'benchmark'

class Webstat
	# In charge of executing probes, measuring time and saving results

	class Probe
		# Initialize probe with following options:
		# 	uri - URI object
		# 	args - Hash of parameters
		# Arguments currently accepted:
		# 	method - HTTP method - :get or :head
		# 	headers - Request headers
		def initialize uri, args = {}
			if !uri.instance_of?(URI::HTTPS) and !uri.instance_of?(URI::HTTP)
				raise ArgumentError, "Expected URI::HTTP or URI::HTTPS got #{uri.class}"
			end
			@uri = uri
			@args = args
		end

		# Execute HTTP probe
		def run
			time = Benchmark.measure do
				begin
					Net::HTTP.start(@uri.host, @uri.port, use_ssl: @uri.scheme == 'https') do |http|
						request =
							case @args[:method]
							when :head
								Net::HTTP::Head.new @uri
							else
								Net::HTTP::Get.new @uri
							end
						@args[:headers].each{ |k,v| request[k] = v } if @args[:headers]
						@response = http.request request
					end
				rescue => err
					@response = nil
					@error = err
				end
			end
			@time = @response.nil? ? nil : time
			@response
		end

		# Probe status returns HTTP status code or error if occured
		def status_str
			if @response
				@response.code
			elsif @error
				@error.to_s
			end
		end

		# Time required to execute probe
		def time
			@time ? @time.real : nil
		end

		# True if probe has been successfully executed
		def successful?
			!@response.nil?
		end
	end

end

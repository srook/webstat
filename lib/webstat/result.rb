class Webstat
	# Represents results after probing run.
	# Contains data of all executed probes.

	class Result
		# Collection of executed probes
		attr_reader :probes

		# Initialize instance variables
		def initialize
			@probes = []
		end

		# Add Probe to results collection.
		def << probe
			raise ArgumentError, "Expected Webstat::Probe got #{probe.class}" unless probe.instance_of? Webstat::Probe
			@probes << probe
		end

		# Return timing information base on type. Options:
		# 	:avg - Average response time
		def timing type
			case type
			when :avg
				suc_probes = successful_probes
				if suc_probes.empty?
					nil
				else
					suc_probes.reduce(0){ |sum, p| sum + p.time } / suc_probes.size
				end
			end
		end

		# Return array of probes that have executed successfully
		def successful_probes
			@probes.select(&:successful?)
		end
	end

end

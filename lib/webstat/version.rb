# frozen-string-literal: true

class Webstat
	# The Major version number. Only changed for significant changes.
	MAJOR = 0
	# The Minor version number. Changed for minor changes.
	MINOR = 0
	# The Tiny version number. Changed for patches and bug-fixes.
	TINY = 1

	# Version string
	VERSION = [MAJOR, MINOR, TINY].join('.').freeze
end

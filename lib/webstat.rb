# frozen_string_literal: true

require_relative 'webstat/probe'
require_relative 'webstat/result'
require_relative 'webstat/version'

# Base class for Webstat.
# Usage:
# 	# Simple run and list probe results
# 	Webstat.new(options).run.result
#
# 	# Run probes and retrieve information
# 	ws = Webstat.new(options).run
# 	ws.result.timing(:avg) # => Average timing of all executed probes
# 	ws.result.by_status_code # => List of executed probes by status code
class Webstat

	# Array of Webstat::Result object.
	# Contains results from all probing runs.
	attr_reader :result

	# URI used for requests
	attr_reader :uri

	# Additional request parameters
	attr_reader :request_args

	# Creates new Webstat object. Available options:
	# 	:delay - Delay between each request (in seconds) (default 10)
	# 	:duration - Total duration of probing run (default 60)
	# 	:domain - Domain to probe
	# 	:port - Port to probe
	# 	:path - Path to probe
	# 	:ssl - Use SSL
	# 	:method - HTTP method - :get or :head (default get)
	# 	:headers - Request headers
	def initialize args = {}
		@delay = args[:delay] || 10
		@duration = args[:duration] || 60

		domain = args[:domain] || 'gitlab.com'
		ssl = args[:ssl].nil? ? true : args[:ssl]

		uri_args = { host: domain, path: args[:path], port: args[:port] }
		@uri = ssl ? URI::HTTPS.build(uri_args) : URI::HTTP.build(uri_args)
		@request_args = {
			method: args[:method] || :get,
			headers: { 'User-Agent' => "Webstat/#{Webstat::VERSION}" }.merge(args[:headers] || {}),
		}
	end

	# Run probe and collect response results
	def run
		@result = Result.new

		run_probe = true
		start_time = Time.now
		while run_probe
			probe = Probe.new @uri, @request_args
			probe.run
			@result << probe

			run_probe = (Time.now - start_time).to_f + @delay < @duration
			sleep @delay if run_probe
		end
		@result
	end

end

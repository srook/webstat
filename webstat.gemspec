require File.expand_path("../lib/webstat/version", __FILE__)

WEBSTAT_GEMSPEC = Gem::Specification.new do |s|
	s.name = "webstat"
	s.version = Webstat::VERSION
	s.summary = "Simple toolkit for checking website status"
	s.description = "Simple toolkit for probing website and gathering timing information"
	s.author = "Saša Rošić"
	s.email = "rosic.sa@gmail.com"
	s.homepage = "https://gitlab.com/srook/webstat"
	s.license = 'MIT'

	s.required_ruby_version = ">= 2.3.0"

	s.require_path = "lib"
	s.files = %w[bin/webstat] + Dir["{spec,lib}/**/*"]

	s.bindir = 'bin'
	s.executables << 'webstat'

	s.add_development_dependency "minitest", '~> 5.11'
	s.add_development_dependency "webmock", '~> 3.4'
end

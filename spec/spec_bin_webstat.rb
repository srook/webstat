require 'rbconfig'
require_relative 'spec_helper'

RUBY = File.join(RbConfig::CONFIG['bindir'], RbConfig::CONFIG['RUBY_INSTALL_NAME'])
OUTPUT = "spec/spec-webstat-#{$$}.log"

describe "bin/webstat" do
	def bin args
		cmd = "#{RUBY} -I lib bin/webstat #{args} > #{OUTPUT} 2>&1 "
		system(cmd)
		File.read(OUTPUT)
	end

	after do
		File.exist? OUTPUT
		File.delete OUTPUT rescue nil
	end

	it '--help should print help' do
		output = bin('--help')
		output.must_match Regexp.new("^Webstat #{Webstat::VERSION}")
		output.must_match(/Usage: webstat \[options\]/)
	end

	it 'should print average response or nil' do
		bin('--domain localhost --duration 0').must_match ''
	end

	it '--verbose should print detailed probe information' do
		bin('--verbose --domain localhost --duration 0').must_match(/Request parameters:/)
	end

	it '--domain should set domain' do
		bin('--verbose --domain 127.0.0.1 --duration 0').must_match(/Request parameters:.*?https:\/\/127.0.0.1/)
	end

	it '--port should set port' do
		bin('--verbose --domain localhost --port 1234 --duration 0').must_match(/Request parameters:.*?https:\/\/localhost:1234/)
	end

	it '--path should set path' do
		bin('--verbose --domain localhost --path /index --duration 0').must_match(/Request parameters:.*?https:\/\/localhost\/index/)
	end

	it '--no-ssl should make HTTP request' do
		bin('--verbose --domain localhost --no-ssl --duration 0').must_match(/Request parameters:.*?http:\/\/localhost/)
	end

	it '--method should set HTTP request method' do
		bin('--verbose --domain localhost --method head --duration 0').must_match(/Request parameters: HEAD/)
	end

	it '--delay should set delay between HTTP requests' do
		bin('--verbose --domain localhost --delay 1 --duration 3').must_match(/Requests made: 3/)
	end

	it '--duration should set maximum duration' do
		time = Benchmark.measure do
			bin('--verbose --domain localhost --delay 1 --duration 3')
		end
		assert time.real < 3
	end
end

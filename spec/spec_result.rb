require_relative 'spec_helper'

describe Webstat::Result do
	before do
		set_uris
		add_stub_requests
	end

	describe '#probes' do
		it 'probes should always be set' do
			r = Webstat::Result.new
			assert_instance_of Array, r.probes
		end
	end

	describe '#<<' do
		it 'value must be Webstat::Probe' do
			r = Webstat::Result.new
			proc{ r << 1 }.must_raise ArgumentError
		end

		it 'should add Webstat::Probe to probes' do
			r = Webstat::Result.new
			assert_equal 0, r.probes.size
			r << Webstat::Probe.new(@httpok)
			assert_equal 1, r.probes.size
		end
	end

	describe '#timing' do
		it 'should return nil if probes are empty' do
			r = Webstat::Result.new
			assert_nil r.timing(:avg)
		end

		it 'should return nil if successful probes are empty' do
			r = Webstat::Result.new
			r << Webstat::Probe.new(@exception)
			r.probes.each(&:run)
			assert_nil r.timing(:avg)
		end

		it 'should return number if any successful probes' do
			r = Webstat::Result.new
			r << Webstat::Probe.new(@httpok)
			r.probes.each(&:run)
			assert_instance_of Float, r.timing(:avg)
		end
	end

	describe '#successful_probes' do
		it 'should always return array' do
			assert_instance_of Array, Webstat::Result.new.successful_probes
		end

		it 'should return only successful probes' do
			r = Webstat::Result.new
			r << Webstat::Probe.new(@httpok)
			r << Webstat::Probe.new(@exception)
			r << Webstat::Probe.new(@redirect)

			r.probes.each(&:run)
			assert_equal 2, r.successful_probes.size
		end
	end
end

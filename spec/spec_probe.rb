require_relative 'spec_helper'

describe Webstat::Probe do
	before do
		set_uris
		add_stub_requests
	end

	describe '#initialize' do
		it 'must raise exception on invalid arguments' do
			proc{ Webstat::Probe.new('https://exception.com') }.must_raise ArgumentError
		end
	end

	describe '#run' do
		it 'should return Net::HTTPOK for successful probe' do
			probe = Webstat::Probe.new(@httpok)
			assert_instance_of Net::HTTPOK, probe.run
		end

		it 'should return nil if failed' do
			probe = Webstat::Probe.new(@exception)
			assert_nil probe.run
		end

		it 'should use head method' do
			probe = Webstat::Probe.new(@gitlab, method: :head)
			probe.run
			assert_requested :head, @gitlab
		end
	end

	describe '#status' do
		it 'should return correct http status' do
			probe = Webstat::Probe.new @httpok
			probe.run
			assert_equal probe.status_str, '200'
		end

		it 'should return error text for exception' do
			probe = Webstat::Probe.new(@exception)
			probe.run
			assert_equal probe.status_str, 'Exception from WebMock'
		end
	end

	describe '#time' do
		it 'should set time for valid probe' do
			probe = Webstat::Probe.new(@httpok)
			probe.run
			assert_instance_of Float, probe.time
		end

		it 'should not set time on run exception' do
			probe = Webstat::Probe.new(@exception)
			probe.run
			assert_nil probe.time
		end
	end

	describe '#successful?' do
		it 'should be false on run exception' do
			probe = Webstat::Probe.new @exception
			probe.run
			assert !probe.successful?
		end

		it 'should be true if run successful' do
			probe = Webstat::Probe.new @httpok
			probe.run
			assert probe.successful?
		end
	end
end

require 'benchmark'

require 'minitest/autorun'
require 'minitest/spec'

require 'webmock/minitest'

require_relative '../lib/webstat'

def add_stub_requests
	stub_request(:get, @exception).to_raise(StandardError)
	stub_request(:get, @httpok).to_return(status: 200)
	stub_request(:get, @redirect).to_return(status: 302)
	stub_request(:any, @about_gitlab).to_return(status: 200)
	stub_request(:head, @gitlab).to_return(status: 302)
end

def set_uris
	@exception = URI::HTTPS.build(host: 'exception.com')
	@httpok = URI::HTTPS.build(host: 'httpok.com')
	@redirect = URI::HTTPS.build(host: 'redirect.com')
	@about_gitlab = URI::HTTPS.build(host: 'about.gitlab.com')
	@gitlab = URI::HTTPS.build(host: 'gitlab.com')
end

require_relative 'spec_helper'

describe Webstat::Result do
	before do
		set_uris
		add_stub_requests
	end

	describe '#initialize' do
		it 'should set delay' do
			assert_equal 10_000, Webstat.new(delay: 10_000).instance_variable_get(:@delay)
		end

		it 'should set duration' do
			assert_equal 10_000, Webstat.new(duration: 10_000).instance_variable_get(:@duration)
		end

		it 'should set uri' do
			assert_equal(
				URI::HTTP.build(host: 'httpok.com', path: '/test/path', port: 10_000),
				Webstat.new(domain: 'httpok.com', ssl: false, path: '/test/path', port: 10_000).instance_variable_get(:@uri)
			)
		end

		it 'should set method' do
			assert_equal Webstat.new(method: :head).request_args[:method], :head
		end

		it 'should set headers' do
			assert_equal(
				Webstat.new(headers: { 'User-Agent' => 'Webstat/0.0.0' }).request_args[:headers],
				{ 'User-Agent' => 'Webstat/0.0.0' }
			)
		end
	end

	describe '#run' do
		it 'should add at least one probe to result' do
			ws = Webstat.new duration: 0, domain: 'about.gitlab.com'
			ws.run
			assert_equal 1, ws.result.probes.size
		end

		it 'should run for at least duration - delay' do
			duration = 3
			delay = 0.5
			ws = Webstat.new duration: duration, delay: 0.5, domain: 'about.gitlab.com'
			time = Benchmark.measure do
				ws.run
			end
			assert time.real > (duration - delay)
		end
	end
end
